import numpy as np
import matplotlib.pyplot as plt
import warnings
import sys
from timeit import default_timer as timer
import matplotlib as mpl
import boto3
import botocore
from botocore import UNSIGNED
from botocore.config import Config
import rasterio
from rasterio.plot import show
from rasterio.io import MemoryFile

warnings.filterwarnings('ignore')

BUCKET_NAME = 'elevation-tiles-prod'

# coord_Y = [6, 12]
# coord_X = [13,20]
# zoom = 5

# coord_X = [6, 10]
# coord_Y = [3, 6]
# zoom = 4

#coord_X = [3, 5]
#coord_Y = [1, 3]
#zoom = 3

coord_X = [55, 81]
coord_Y = [27, 50]
zoom = 7

# coord_X = [27, 40]
# coord_Y = [13,25]
# zoom = 6

tiles_x_count = coord_X[1] - coord_X[0] + 1
tiles_y_count = coord_Y[1] - coord_Y[0] + 1


def generate_paths():
    const = "s3://elevation-tiles-prod/geotiff/" + str(zoom) + "/"
    bodies = []
    s3 = boto3.resource('s3', config=Config(signature_version=UNSIGNED))
    for x in range(coord_X[0], coord_X[1] + 1):
        for y in range(coord_Y[0], coord_Y[1] + 1):
            obj = s3.Object("elevation-tiles-prod", f"geotiff/{zoom}/{x}/{y}.tif")
            body = obj.get()['Body'].read()
            bodies.append(body)

    return bodies


def get_geo_elevation_array(byte):
    with MemoryFile(byte) as memfile:
        with memfile.open() as dt:
            data_arr = dt.read()
            return data_arr


def get_raise_value(rdd_array):
    TILE_SIZE = 512
    SUB_TILE_SIZE = 8

    val = np.squeeze(rdd_array)

    tile = np.vsplit(val, TILE_SIZE // SUB_TILE_SIZE)
    tile = np.asarray(np.dsplit(np.asarray(tile), TILE_SIZE // SUB_TILE_SIZE)).swapaxes(0, 1).reshape(-1, SUB_TILE_SIZE,
                                                                                                      SUB_TILE_SIZE)

    minimums = np.amin(tile, axis=(1, 2))
    maximums = np.amax(tile, axis=(1, 2))
    deltas = maximums - minimums

    return deltas.reshape((TILE_SIZE // SUB_TILE_SIZE, TILE_SIZE // SUB_TILE_SIZE))


def to_ranges(rdd_array, bins):
    return np.digitize(rdd_array, bins, right=True) - 1


def plot_save_data(bins):
    import matplotlib as mpl
    cmap = plt.cm.turbo
    bounds = np.linspace(0, 6, 7)
    norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
    height_labels = ["{:.1f}".format(bins[i]) + " - {:.1f}".format(bins[i + 1]) for i in range(len(bins) - 1)]

    plt.imshow(map_all, norm=norm)
    cbar = plt.colorbar()
    cbar.set_ticks(np.arange(0.5, 6.5, 1))
    cbar.set_ticklabels(height_labels)

    plt.savefig('foo.png')


if __name__ == '__main__':
    make_plot = True

    paths = generate_paths()
    start_processing = timer()
    a2 = [get_geo_elevation_array(paths[i]) for i in range(len(paths))]
    a3 = [a2[i].clip(0, np.max(a2[i])) for i in range(len(a2))]
    a4 = [get_raise_value(a3[i]) for i in range(len(a3))]

    max_val = np.amax(a4)
    bins = np.geomspace(1, max_val, 6)
    bins = np.insert(bins, 0, 0)

    a4 = np.asarray([to_ranges(a4[i], bins) for i in range(len(a4))])

    map_rows = []

    for y in range(tiles_x_count):
        map_rows.append(np.vstack(a4[y * tiles_y_count:y * tiles_y_count + tiles_y_count]))

    map_all = np.hstack(map_rows)

    end_processing = timer()
    processing_time = end_processing - start_processing

    print("Processing time: ", processing_time)
    plot_save_data(bins)

