import rasterio
from rasterio.plot import show
from rasterio.io import MemoryFile
import time
from datetime import datetime
import numpy as np
import sys
import pyspark
from pyspark.sql import SparkSession
from pyspark import SparkContext
from pyspark.sql import SQLContext
import math
from matplotlib import pyplot as plt
from timeit import default_timer as timer
from pyspark.sql.types import *
from pyspark.sql.functions import col, struct, udf

sc = SparkContext()
spark = SparkSession(sc)

from math import log, tan, pi
zoom = 9
bounds = [71.0, -24.0, 36.0, 50.0]
def get_tiles_no(zoom, lat, lon):
    x1, y1 = lon * pi / 180, lat * pi / 180
    x2, y2 = x1, log(tan(0.25 * pi + 0.5 * y1))
    tiles, diameter = 2 ** zoom, 2 * pi
    x3, y3 = int(tiles * (x2 + pi) / diameter), int(tiles * (pi - y2) / diameter)
    return x3, y3

x,y = get_tiles_no(zoom, *bounds[:2])
x1,y1 = get_tiles_no(zoom, *bounds[2:])

coord_X = [x, x1]
coord_Y = [y, y1]

tiles_x_count = coord_X[1]-coord_X[0]+1
tiles_y_count = coord_Y[1]-coord_Y[0]+1

def generate_paths():
    paths = []
    
    for x in range(coord_X[0],coord_X[1]+1):
        for y in range(coord_Y[0],coord_Y[1]+1):
            paths.append(f"s3://elevation-tiles-prod/geotiff/{zoom}/{x}/{y}.tif")
    return paths


def get_geo_elevation_array(byte):
    """
    Function to obtain height from bytes data
    """
    with MemoryFile(bytes(byte)) as memfile:
        with memfile.open() as dt:
            data_arr = np.squeeze(dt.read())
            return data_arr.tolist()
get_geo=udf(get_geo_elevation_array, ArrayType(ArrayType(IntegerType())))

def zero_water_area(arr):
    """
    Removing sea and oceans height
    """
    abc = np.asarray(arr)
    abc = np.clip(abc, 0, None)
    return abc.tolist()
zerowater=udf(zero_water_area, ArrayType(ArrayType(IntegerType())))

def get_raise_value(arr):
    TILE_SIZE=512
    SUB_TILE_SIZE=(2**(zoom-3))
    val = np.asarray(arr)
    if(val.shape[0] != TILE_SIZE):
       SUB_TILE_SIZE=int(SUB_TILE_SIZE//(TILE_SIZE//val.shape[0]))
    val = val.reshape(TILE_SIZE//SUB_TILE_SIZE, SUB_TILE_SIZE, -1, SUB_TILE_SIZE).swapaxes(1, 2).reshape(-1, SUB_TILE_SIZE, SUB_TILE_SIZE)
    #tile = np.vsplit(val, TILE_SIZE//SUB_TILE_SIZE)
    #tile = np.asarray(np.dsplit(np.asarray(tile), TILE_SIZE//SUB_TILE_SIZE)).swapaxes(0,1).reshape(-1,SUB_TILE_SIZE, SUB_TILE_SIZE)
    deltas = np.mean(np.mean(np.abs(np.gradient(val, axis=(1,2))), axis=0), axis=(1,2))

    return deltas.reshape((int(val.shape[0]**(1/2)), int(val.shape[0]**(1/2)))).tolist()

raise_value=udf(get_raise_value, ArrayType(ArrayType(IntegerType())))

def to_ranges(arr):
    bins = np.geomspace(1, 4500, 6)
    bins = np.insert(bins, 0, 0)
    a = np.digitize(arr, bins, right=True)-1
    return a.tolist()

digi=udf(to_ranges, ArrayType(ArrayType(IntegerType())))


def plot_save_data(bins):
    """
    Simple plotting and plot saving function
    """
    import matplotlib as mpl
    cmap = plt.cm.turbo
    bounds = np.linspace(0, 6, 7)
    norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
    #print(bins)
    height_labels = ["{:.1f}".format(bins[i]) + " - {:.1f}".format(bins[i+1])  for i in range(len(bins)-1)]
    #height_labels.insert(0, "0.0 - 1.0") 

    plt.imshow(map_all, norm=norm)
    cbar = plt.colorbar()
    cbar.set_ticks(np.arange(0.5, 6.5, 1))
    cbar.set_ticklabels(height_labels)

    plt.savefig('foo.png')


if __name__ == "__main__":
    paths = generate_paths()
    df=spark.read.format("binaryFile").load(paths).orderBy("path")
    
    start_processing = timer()
    df1=df.select(get_geo(df.content).alias("geo"))
    df2=df1.select(zerowater(df1.geo).alias("zero"))
    df3=df2.select(raise_value(df2.zero).alias("rvalue"))
    df4=df3.select(digi(df3.rvalue).alias("d"))
    
    tiles_digitized = df4.collect()
    tiles_digitized = np.asarray([tiles_digitized[i].asDict()["d"] for i in range(len(tiles_digitized))])
    map_rows=[]

    for y in range(tiles_x_count):
        map_rows.append(np.vstack(tiles_digitized[y*tiles_y_count:y*tiles_y_count+tiles_y_count]))

    map_all = np.hstack(map_rows)

    end_processing = timer()
    processing_time = end_processing - start_processing

    with open(f"time_zoom{zoom}.txt", "w") as file:
        file.write(f'{processing_time} sek\n{datetime.now()}')
    
    bins = np.geomspace(1, 4500, 6)
    bins = np.insert(bins, 0, 0)
    plot_save_data(bins)
