# Intelligent Measuring System
Project at Gdansk University of Technology, Faculty od Electronics, Telecommunications and Informatics

## Description
Design and implement a measurement system that analyzes data on terrain height variation at points with the highest growth (Europe). The height increase in a given location should be measured on the basis of 10 measurement points. 
Find 5 groups of the sink basing on the average growth of altitude. Put detected areas on the map.

## Technologies
Our project is supposed to be using cloud computing technologies - in this case Amazon Web Services, later called AWS. This platform allows fast processing of huge datasets. 
Performance of AWS was later compared to our PCs' performance. Local platform consists of the following components.
Intel Core I7 6700HQ CPU
16GB of DDR4 memory,
Windows 10 64bit.

AWS' performance depended on the following parameters:
EMR 5.32.0 cluster on AWS
Spark 2.4.7 on Hadoop 2.10.1 YARN and Zeppelin 0.8.2
m4.xlarge AWS cluster configuration


## Dataset
Source dataset is located here: https://registry.opendata.aws/terrain-tiles/. 
Data is splitted into tiles. There are 15 different zoom values available for user to choose. 
The are also 4 different formats available to choose for user: terrarium, normal, skadi and geotiff.

Our team decided to use geotiff data, which is minimising tranfer costs. It is also a good choice for our project thanks to its ease of use in analytical purposes.
Height values are provided from start in geotiff format, so we can use them for computational purposes easily.


## Speed of growth
The main objective of our project was to compute and present the speed of land growth. 
This task was accomplished by calculating differences between the highest and lowest points in matrix containing of 4x4 pixel cells. 
Each difference is then assigned to one of five ranges and displayed on a map with corresponding color. 



## Results
Our team prepared two software solutions. One of which was written in pure-python form and second one was based on python with pyspark 2.4.7

Final result is presented in the picture below. There are 5 ranges which clearly show - 5 groups based on growth of altitude:

![Final plot](zoom7_res.png)

Time results for pure-python version:
- PC:
    - zoom 5 -> 29.12408710000004  s
- AWS Cluster 3 Cores:
    - zoom 5 -> 8.776827306999962  s

Time results for python-pyspark version:
- PC:
    - zoom 3 -> 14.85817440000003  s
    - zoom 5 -> 17.46864329999997  s
    - zoom 6 -> 14.82514099999997  s 
    - zoom 7 -> 18.65432669999999  s
- AWS Cluster 3 Cores:
    - zoom 3 -> 6.289440343000024  s
    - zoom 5 -> 6.063978649999626  s
    - zoom 6 -> 3.639790475997870  s
    - zoom 7 -> 6.960875299999316  s
- AWS Cluster 2 Cores:
    - zoom 3 -> 7.139147987000001  s
    - zoom 5 -> 7.337260982999965  s
    - zoom 6 -> 4.646685597999976  s
    - zoom 7 -> 8.578573541999958  s

UPDATE
- AWS Cluster 3 Cores:
    - zoom 11-> 5414s  - DataFrame
    - zoom 11-> 20978s - rdd


## Conclusion
Our project delivered valuable data on Europe's terrain height variation. Results are gathered and presented on a color map containing 5 ranges of height's growth speed.
Precision of gathered results can be adjusted by setting specified zoom parameters. 

Our team sees great possibilities of this technology. Potentiality of receiving precise values from computations on huge sets of data in quite short time delivers great chances for making our world a better place. This technique is reliable and gives repeated results, which is very important for volatile and sensitive tasks. 
Comparison of performance of AWS Cluster and a mediocre notebook also gave clear conclusions. Personal computers ale limited by their hardware. We can upgrade them, but this takes time and is not always possible. On the other hand cloud computing gives an opportunity to adjust its resources to our needs. In just a few seconds we can choose a different amount of nodes or type of a computing unit.   
Chosen configuration of AWS Cluster turned out to be approximately 2-3x faster than our notebook. AWS' speed depended heavily on the amount of it's computational cores. We compared AWS Clusters with 2 (master+core) and 3 cores (master+2 cores) and saw an advante of 13-28% in speed of 3 core cluster compared to 2 core. Further improvements were registered when using PySpark version of our code which equaled between 30 and  60 percent. DataFrame brought another noticeable enhancement. Processing time decreased by over 70% while using DataFrame compared to rdd.
We appreciate cloud computing for its flexibility and cost-effectiveness. It is ease to use and always ready to improve our workflow when we need it.



#### Project developers :
* Rafał Okuński
* Łukasz Antoniszyn
* Łukasz Pawłowski
