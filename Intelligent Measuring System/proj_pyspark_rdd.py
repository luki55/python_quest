import rasterio
from rasterio.plot import show
from rasterio.io import MemoryFile
import time
from datetime import datetime
import numpy as np
import sys
import pyspark
from pyspark.sql import SparkSession
from pyspark import SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.types import StringType, IntegerType,StructType,FloatType,StructField
import math
from matplotlib import pyplot as plt
from timeit import default_timer as timer
import boto3
import botocore
from botocore import UNSIGNED
from botocore.config import Config


sc= SparkContext()
spark = SparkSession(sc)

from math import log, tan, pi
zoom = 5
bounds = [71.0, -24.0, 36.0, 50.0]
def get_tiles_no(zoom, lat, lon):
    x1, y1 = lon * pi / 180, lat * pi / 180
    x2, y2 = x1, log(tan(0.25 * pi + 0.5 * y1))
    tiles, diameter = 2 ** zoom, 2 * pi
    x3, y3 = int(tiles * (x2 + pi) / diameter), int(tiles * (pi - y2) / diameter)
    return x3, y3

x,y = get_tiles_no(zoom, *bounds[:2])
x1,y1 = get_tiles_no(zoom, *bounds[2:])

coord_X = [x, x1]
coord_Y = [y, y1]

tiles_x_count = coord_X[1]-coord_X[0]+1
tiles_y_count = coord_Y[1]-coord_Y[0]+1
                                   
print(coord_X)
print(coord_Y)

def generate_paths():
    paths = []
    
    for x in range(coord_X[0],coord_X[1]+1):
        for y in range(coord_Y[0],coord_Y[1]+1):
            paths.append(f"geotiff/"+str(zoom)+"/"f"{x}/{y}.tif")
    return paths


def generate_paths2(path):
    """
    Function to read byte data from S3 geotiff tiles
    """
    #bodies = []
    s3 = boto3.resource('s3')
    # for x in range(coord_X[0],coord_X[1]+1):
    #     for y in range(coord_Y[0],coord_Y[1]+1):
    #         #paths.append(f"{const}{x}/{y}.tif")
    print(path)
    obj = s3.Object("elevation-tiles-prod", path)
    body = obj.get()['Body'].read()
            #print(type(body))

    return body

def get_geo_elevation_array(byte):
    """
    Function to obtain height from bytes data
    """
    with MemoryFile(byte) as memfile:
        with memfile.open() as dt:
            data_arr = dt.read()
            return data_arr

def zero_water_area(rdd_array):
    """
    Removing sea and oceans height
    """
    return rdd_array.clip(0, np.max(rdd_array))

        
def get_raise_value(rdd_array):
    """
    Getting raise in terrain for subtiles
    """
    val = rdd_array[0]
    TILE_SIZE=512
    SUB_TILE_SIZE=(2**(zoom-3))
    if(val.shape[0] != TILE_SIZE):
       SUB_TILE_SIZE=int(SUB_TILE_SIZE//(TILE_SIZE//val.shape[0]))
    val = val.reshape(TILE_SIZE//SUB_TILE_SIZE, SUB_TILE_SIZE, -1, SUB_TILE_SIZE).swapaxes(1, 2).reshape(-1, SUB_TILE_SIZE, SUB_TILE_SIZE)
    #tile = np.vsplit(val, TILE_SIZE//SUB_TILE_SIZE)
    #tile = np.asarray(np.dsplit(np.asarray(tile), TILE_SIZE//SUB_TILE_SIZE)).swapaxes(0,1).reshape(-1,SUB_TILE_SIZE, SUB_TILE_SIZE)
    
    minimums = np.amin(val, axis=(1, 2))
    maximums = np.amax(val, axis=(1, 2))
    deltas = maximums-minimums

    return deltas.reshape((int(val.shape[0]**(1/2)), int(val.shape[0]**(1/2))))

def to_ranges(rdd_array, bins):
    """
    Heights to 5 ranges
    """
    return np.digitize(rdd_array, bins, right=True)-1

def plot_save_data(bins):
    """
    Simple plotting and plot saving function
    """
    import matplotlib as mpl
    cmap = plt.cm.turbo
    bounds = np.linspace(0, 6, 7)
    norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
    #print(bins)
    height_labels = ["{:.1f}".format(bins[i]) + " - {:.1f}".format(bins[i+1])  for i in range(len(bins)-1)]
    #height_labels.insert(0, "0.0 - 1.0") 

    plt.imshow(map_all, norm=norm)
    cbar = plt.colorbar()
    cbar.set_ticks(np.arange(0.5, 6.5, 1))
    cbar.set_ticklabels(height_labels)

    plt.savefig('foo_rdd.png')

## ======

if __name__ == "__main__":
    start_processing1 = timer()
    paths = generate_paths()
    #rdd = sc.binaryFiles(",".join(paths))
    #rdd2 = rdd.map(lambda x: bytes(x[1]))
    
    rdd = sc.parallelize(paths)
    rdd = rdd.map(lambda x: generate_paths2(x))
    end_processing1 = timer()
    processing_time1 = end_processing1 - start_processing1
    start_processing = timer()
    rdd3 = rdd.map(lambda x: get_geo_elevation_array(x))
    rdd4 = rdd3.map(lambda x: zero_water_area(x))
    rdd5 = rdd4.map(lambda x: get_raise_value(x))

    rdd_max = rdd5.map(lambda x: np.max(x)).max()
    bins = np.geomspace(1, rdd_max, 6)
    bins = np.insert(bins, 0, 0)
    rdd_ranges = rdd5.map(lambda x: to_ranges(x, bins))

    tiles_ranges = rdd_ranges.collect()
    map_rows=[]

    for y in range(tiles_x_count):
        map_rows.append(np.vstack(tiles_ranges[y*tiles_y_count:y*tiles_y_count+tiles_y_count]))

    map_all = np.hstack(map_rows)
    end_processing = timer()
    processing_time = end_processing - start_processing
    
    with open(f"time_zoom{zoom}_rdd.txt", "w") as file:
        file.write(f'{processing_time} sek\n{datetime.now()}')
    plot_save_data(bins)

    print("Downloading time: ", processing_time1)
    print("Processing time: ", processing_time)
