import pytest
import logika
test=logika.main()

@pytest.mark.parametrize('wojewodztwo,rok,plec,option,result',
                         [
                             ("Podkarpackie", 2018, "wszyscy", 0 ,19450.88888888889),
                             ("Mazowieckie", 2010, "mężczyźni", 0 ,22265),
                             ("Opolskie", 2013, "kobiety", 0 ,4546.75)
                         ]
                         )
def test_srednia(wojewodztwo,rok,plec,option,result):
    assert test.srednia(wojewodztwo,rok,plec,option)==result


@pytest.mark.parametrize('wojewodztwo,rok,plec,option,result',
                         [
                             ("Łódzkie",2018,"wszyscy",0,[81.91362239672243, 76.0, 80.0862314033924, 80.55072319697798,
                                                          71.3138021530716, 73.8625971650663, 79.89269601268138, 79.46753968744208,
                                                          79.29492910972027]),
                             ("Małopolskie", 2013,"mężczyźni", 0,
                              [81.15865701119158, 77.39290838286534, 82.34488722346329, 82.3551570858133]),
                             ("Kujawsko-pomorskie",2010,"kobiety",0,[82.76607747273411])
                         ]
                         )

def test_zdawalnosc(wojewodztwo,rok,plec,option,result):
    assert test.zdawalnosc(wojewodztwo,rok,plec,option)==result




@pytest.mark.parametrize('rok,plec,option,result',
                         [
                            (2012,"wszyscy",0,"Małopolskie"),
                            (2010,"kobiety",0,"Małopolskie"),
                            (2015,"mężczyźni",0,"Małopolskie")
                         ]
                         )
def test_najlepsza_zdawalnosc(rok,plec,option,result):
    assert test.kto_najlepsza_zdawalnosc(rok,plec,option)==result


@pytest.mark.parametrize('option,plec,wojA,wojB,result',
                         [
                            (0,"wszyscy", "Śląskie","Pomorskie",
                             ['Śląskie', 'Śląskie', 'Śląskie', 'Śląskie', 'Pomorskie', 'Śląskie', 'Pomorskie', 'Pomorskie', 'Śląskie']),
                            (0,"kobiety","Kujawsko-pomorskie","Świętokrzyskie",
                             ['Kujawsko-pomorskie', 'Świętokrzyskie', 'Kujawsko-pomorskie', 'Kujawsko-pomorskie', 'Świętokrzyskie',
                              'Świętokrzyskie', 'Świętokrzyskie', 'Świętokrzyskie', 'Świętokrzyskie']),
                            (0, "mężczyźni","Lubelskie","Lubuskie",
                             ['Lubuskie', 'Lubuskie', 'Lubuskie', 'Lubuskie', 'Lubuskie', 'Lubuskie', 'Lubuskie', 'Lubuskie', 'Lubelskie'])
                         ]
                         )

def test_porownaj(option,plec,wojA,wojB,result):
    assert test.porownaj(wojA,wojB,plec,option)==result

@pytest.mark.parametrize('option,plec,result',
                         [
                            (0, "wszyscy",['Dolnośląskie', 'Dolnośląskie', 'Dolnośląskie', 'Kujawsko-pomorskie', 'Kujawsko-pomorskie',
                                           'Kujawsko-pomorskie', 'Lubelskie', 'Lubelskie', 'Lubelskie', 'Lubuskie', 'Lubuskie', 'Lubuskie', 'Lubuskie', 'Łódzkie',
                                           'Łódzkie', 'Łódzkie', 'Łódzkie', 'Małopolskie', 'Małopolskie', 'Mazowieckie', 'Mazowieckie', 'Mazowieckie', 'Opolskie', 'Opolskie',
                                           'Opolskie', 'Podkarpackie', 'Podkarpackie', 'Podkarpackie', 'Podlaskie', 'Podlaskie', 'Podlaskie', 'Pomorskie', 'Pomorskie', 'Pomorskie',
                                           'Pomorskie', 'Śląskie', 'Śląskie', 'Śląskie', 'Świętokrzyskie', 'Świętokrzyskie', 'Świętokrzyskie', 'Warmińsko-Mazurskie', 'Warmińsko-Mazurskie',
                                           'Warmińsko-Mazurskie', 'Wielkopolskie', 'Wielkopolskie', 'Wielkopolskie', 'Zachodniopomorskie', 'Zachodniopomorskie', 'Zachodniopomorskie']),

                            (0,"kobiety",['Dolnośląskie', 'Dolnośląskie', 'Dolnośląskie', 'Kujawsko-pomorskie', 'Kujawsko-pomorskie',
                                          'Kujawsko-pomorskie', 'Lubelskie', 'Lubelskie', 'Lubelskie', 'Lubuskie', 'Lubuskie', 'Lubuskie', 'Łódzkie',
                                          'Łódzkie', 'Łódzkie', 'Małopolskie', 'Małopolskie', 'Mazowieckie', 'Mazowieckie', 'Mazowieckie', 'Opolskie', 'Opolskie',
                                          'Opolskie', 'Podkarpackie', 'Podkarpackie', 'Podkarpackie', 'Podlaskie', 'Podlaskie', 'Podlaskie', 'Pomorskie', 'Pomorskie',
                                          'Pomorskie', 'Pomorskie', 'Śląskie', 'Śląskie', 'Śląskie', 'Świętokrzyskie', 'Świętokrzyskie', 'Świętokrzyskie', 'Warmińsko-Mazurskie',
                                          'Warmińsko-Mazurskie', 'Warmińsko-Mazurskie', 'Wielkopolskie', 'Wielkopolskie', 'Wielkopolskie', 'Zachodniopomorskie', 'Zachodniopomorskie',
                                          'Zachodniopomorskie']),

                            (0,"mężczyźni",['Dolnośląskie', 'Dolnośląskie', 'Dolnośląskie', 'Dolnośląskie', 'Kujawsko-pomorskie', 'Kujawsko-pomorskie',
                                           'Kujawsko-pomorskie', 'Lubelskie', 'Lubelskie', 'Lubelskie', 'Lubuskie', 'Lubuskie', 'Lubuskie', 'Lubuskie', 'Lubuskie',
                                           'Łódzkie', 'Łódzkie', 'Łódzkie', 'Łódzkie', 'Małopolskie', 'Małopolskie', 'Małopolskie', 'Mazowieckie', 'Mazowieckie', 'Mazowieckie',
                                           'Mazowieckie', 'Opolskie', 'Opolskie', 'Opolskie', 'Podkarpackie', 'Podkarpackie', 'Podkarpackie', 'Podkarpackie', 'Podlaskie', 'Podlaskie',
                                           'Podlaskie', 'Podlaskie', 'Pomorskie', 'Pomorskie', 'Pomorskie', 'Pomorskie', 'Śląskie', 'Śląskie', 'Śląskie', 'Świętokrzyskie', 'Świętokrzyskie',
                                           'Świętokrzyskie', 'Warmińsko-Mazurskie', 'Warmińsko-Mazurskie', 'Warmińsko-Mazurskie', 'Warmińsko-Mazurskie', 'Wielkopolskie', 'Wielkopolskie',
                                           'Wielkopolskie', 'Wielkopolskie', 'Wielkopolskie', 'Zachodniopomorskie', 'Zachodniopomorskie', 'Zachodniopomorskie'])

                         ]
                         )

def test_recesja(option,plec,result):
    assert test.recesja(plec=plec,option=option)==result