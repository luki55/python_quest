import pomiar


class main(object):
    def __init__(self):
        self.lista = []

    def wczytaj(self):
        listanew=[]
        f = open(".\\dataset.csv", "r").read()
        lines = f.split('\n')

        for line in lines:
            line = line.encode("windows-1250").decode("windows-1250")
            line = line.replace(';', ' ')
            line = line.split()

            if (len(line) > 1):
                if (line[0] != "Terytorium"):
                    nowe = pomiar.Pomiar()
                    nowe.nazwa = line[0]
                    nowe.przystapilo_zdalo = line[1]
                    nowe.plec = line[2]
                    nowe.rok = int(line[3])
                    nowe.liczba_osob = int(line[4])
                    listanew.append(nowe)
        self.lista=listanew

    def srednia(self, wojewodztwo, rok, plec="wszyscy",option=1):
        self.wczytaj()
        suma = 0.0
        lat = 0.0
        for i in range(len(self.lista)):
            if ((self.lista[i].nazwa == wojewodztwo) and (self.lista[i].rok <= rok) and (
                    self.lista[i].przystapilo_zdalo == 'przystąpiło')):
                if (plec == "wszyscy"):
                    suma = suma + self.lista[i].liczba_osob
                    lat = lat + 0.5
                elif (self.lista[i].plec == plec):
                    suma = suma + self.lista[i].liczba_osob
                    lat = lat + 1

        srednia = suma / lat
        if (option == 1):
            print(srednia)
        return (float)(srednia)

    def zdawalnosc(self, wojewodztwo, rok, plec="wszyscy", option=1):
        self.wczytaj()
        przystapilo = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        zdalo = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        wyniki = []
        for i in range(len(self.lista)):
            if ((self.lista[i].nazwa == wojewodztwo) and (self.lista[i].rok <= rok) and (
                    self.lista[i].przystapilo_zdalo == 'przystąpiło')):
                if (plec == "wszyscy"):
                    przystapilo[self.lista[i].rok - 2010] = przystapilo[self.lista[i].rok - 2010] + self.lista[
                        i].liczba_osob
                elif (self.lista[i].plec == plec):
                    przystapilo[self.lista[i].rok - 2010] = przystapilo[self.lista[i].rok - 2010] + self.lista[
                        i].liczba_osob

            if ((self.lista[i].nazwa == wojewodztwo) and (self.lista[i].rok <= rok) and (
                    self.lista[i].przystapilo_zdalo == 'zdało')):
                if (plec == "wszyscy"):
                    zdalo[self.lista[i].rok - 2010] = zdalo[self.lista[i].rok - 2010] + self.lista[i].liczba_osob
                elif (self.lista[i].plec == plec):
                    zdalo[self.lista[i].rok - 2010] = zdalo[self.lista[i].rok - 2010] + self.lista[i].liczba_osob

        for i in range(rok - 2010 + 1):
            wyniki.append(100 * zdalo[i] / przystapilo[i])
            if (option == 1):
                print("W roku ", i + 2010, "w wojewodztwie", wojewodztwo, " zdalo ", wyniki[i], " % ")

        return wyniki

    def kto_najlepsza_zdawalnosc(self, rok, plec="wszyscy",option=1):
        self.wczytaj()
        najlepsze = None
        najlepsze_zdawalnosc = 0
        counter = 0
        aktualne = 0
        przystapilo = 0
        zdalo = 0

        for i in range(len(self.lista)):
            if (plec == "wszyscy"):
                if (self.lista[i].nazwa != "Polska"):
                    if (self.lista[i].rok == rok):
                        if (self.lista[i].przystapilo_zdalo == "przystąpiło"):
                            przystapilo = przystapilo + self.lista[i].liczba_osob
                            counter = counter + 1
                        elif (self.lista[i].przystapilo_zdalo == "zdało"):
                            zdalo = zdalo + self.lista[i].liczba_osob
                            counter = counter + 1
                        if (((counter % 4) == 0) and (counter != 0)):
                            aktualne = 100 * zdalo / przystapilo
                            zdalo = 0
                            przystapilo = 0
                            if (aktualne > najlepsze_zdawalnosc):
                                najlepsze_zdawalnosc = aktualne
                                najlepsze = self.lista[i - 1].nazwa

            elif (plec == self.lista[i].plec):
                if (self.lista[i].nazwa != "Polska"):
                    if (self.lista[i].rok == rok):
                        if (self.lista[i].przystapilo_zdalo == "przystąpiło"):
                            przystapilo = przystapilo + self.lista[i].liczba_osob
                            counter = counter + 1
                        elif (self.lista[i].przystapilo_zdalo == "zdało"):
                            zdalo = zdalo + self.lista[i].liczba_osob
                            counter = counter + 1
                        if (((counter % 2) == 0) and (counter != 0)):
                            aktualne = 100 * zdalo / przystapilo
                            zdalo = 0
                            przystapilo = 0
                            if (aktualne > najlepsze_zdawalnosc):
                                najlepsze_zdawalnosc = aktualne
                                najlepsze = self.lista[i - 1].nazwa

        if(option==1):
            print("Najlepsze", rok, najlepsze, "Wynik", najlepsze_zdawalnosc)
        return najlepsze

    def recesja(self, plec="wszyscy",option=1):
        self.wczytaj()
        poprzednie_wojewodztwo = "Polska"
        wojewodztwa = []
        retwoj=[]
        count=0

        for i in range(len(self.lista)):
            if (poprzednie_wojewodztwo != self.lista[i].nazwa):
                wojewodztwa.append(self.lista[i].nazwa)
                poprzednie_wojewodztwo = self.lista[i].nazwa

        for i in range(len(wojewodztwa)):
            wyniki = self.zdawalnosc(wojewodztwa[i], 2018, plec=plec, option=0)
            for j in range(len(wyniki) - 1):
                if (wyniki[j] > wyniki[j + 1]):
                    retwoj.append(wojewodztwa[i])
                    count = count+1
                    print(wojewodztwa[i])
                    if(option==1):
                        print("Recesja w wojewodztwie ", wojewodztwa[i], "W latach", 2010 + j, 2011 + j)
        #print(retwoj)
        print("licznik", count)
        return retwoj

    def porownaj(self, wojewodztwoA, wojewodztwoB, plec="wszyscy",option=1):
        self.wczytaj()
        wynikiA = self.zdawalnosc(wojewodztwoA, 2018, plec=plec, option=0)
        wynikiB = self.zdawalnosc(wojewodztwoB, 2018, plec=plec, option=0)
        best=[]

        for i in range(len(wynikiA)):
            if (wynikiA[i] > wynikiB[i]):
                if(option==1):
                    print("W roku ", 2010 + i, " lepsze bylo wojewodztwo ", wojewodztwoA)
                best.append(wojewodztwoA)
            elif (wynikiA[i] < wynikiB[i]):
                if (option == 1):
                    print("W roku ", 2010 + i, " lepsze bylo wojewodztwo ", wojewodztwoB)
                best.append(wojewodztwoB)
            else:
                if (option == 1):
                    print("W roku ", 2010 + i, " byl remis ")
        return best


