import sys
import time
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import random
import winsound
import matplotlib.pyplot as plt


class Example(QWidget):

    def __init__(self):
        super().__init__()
        self.timer = QTimer()
        self.timer.timeout.connect(self.stan)
        self.timer.start(10)
        self.start=time.time()
        self.end = time.time()-self.start
        self.counter = 0
        self.numer=0
        self.czy_test=0
        self.picture = "./info.png"
        self.wyniki=[self.end,self.end,self.end,self.end,self.end,self.end,self.end,self.end,self.end,self.end]
        self.probki=[1,2,3,4,5,6,7,8,9,10]
        self.initUI()

    def result(self):
        plt.plot(self.probki,self.wyniki)
        plt.xlabel("numer testu")
        plt.ylabel("czas")
        plt.show()

    def reset(self):
        self.counter = 0
        self.numer=0
        self.start = time.time()
        self.end = time.time() - self.start
        self.wyniki = [self.end, self.end, self.end, self.end, self.end, self.end, self.end, self.end, self.end,
                       self.end]
        self.picture = "info.png"
        self.update()

    def sheep(self):
        if self.czy_test == 0:
            if self.numer%6 == 0:
                self.end = time.time() - self.start
                self.wyniki[self.counter] = self.end
                self.counter = self.counter + 1
                if (self.counter < 10):
                    self.tura()
                else:
                    self.result()
        else:
            if self.numer % 6 == 0:
                if (self.numer < 12):
                    self.numer=self.numer+1
                    self.test()
                else:
                    self.reset()

    def cow(self):
        if self.czy_test==0:
            if self.numer%6 == 1:
                self.end = time.time() - self.start
                self.wyniki[self.counter] = self.end
                self.counter = self.counter + 1
                if(self.counter<10):
                    self.tura()
                else:
                    self.result()
        else:
            if self.numer % 6 == 1:
                if (self.numer < 12):
                    self.numer = self.numer + 1
                    self.test()
                else:
                    self.reset()

    def dog(self):
        if self.czy_test==0:
            if self.numer%6 == 2:
                self.end = time.time() - self.start
                self.wyniki[self.counter] = self.end
                self.counter = self.counter + 1
                if(self.counter<10):
                    self.tura()
                else:
                    self.result()
        else:
            if self.numer % 6 == 2:
                if (self.numer < 12):
                    self.numer = self.numer + 1
                    self.test()
                else:
                    self.reset()

    def horse(self):
        if self.czy_test==0:
            if self.numer%6 == 3:
                self.end = time.time() - self.start
                self.wyniki[self.counter] = self.end
                self.counter = self.counter + 1
                if(self.counter<10):
                    self.tura()
                else:
                    self.result()
        else:
            if self.numer % 6 == 3:
                if (self.numer < 12):
                    self.numer = self.numer + 1
                    self.test()
                else:
                    self.reset()

    def chicken(self):
        if self.czy_test==0:
            if self.numer%6 == 4:
                self.end = time.time() - self.start
                self.wyniki[self.counter] = self.end
                self.counter = self.counter + 1
                if(self.counter<10):
                    self.tura()
                else:
                    self.result()
        else:
            if self.numer % 6 == 4:
                if (self.numer < 12):
                    self.numer = self.numer + 1
                    self.test()
                else:
                    self.reset()

    def pig(self):
        if self.czy_test==0:
            if self.numer%6 == 5:
                self.end = time.time() - self.start
                self.wyniki[self.counter] = self.end
                self.counter = self.counter + 1
                if(self.counter<10):
                    self.tura()
                else:
                    self.result()
        else:
            if self.numer % 6 == 5:
                if (self.numer < 12):
                    self.numer = self.numer + 1
                    self.test()
                else:
                    self.reset()

    def test(self):
        self.czy_test=1
        if self.numer == 0:
            self.picture = "sheep.jpg"
        elif self.numer == 1:
            self.picture = "cow.jpg"
        elif self.numer == 2:
            self.picture = "dog.jpg"
        elif self.numer == 3:
            self.picture = "horse.jpg"
        elif self.numer == 4:
            self.picture = "chicken.png"
        elif self.numer == 5:
            self.picture = "pig.jpg"
        elif self.numer == 6:
            self.picture = "sound.png"
            winsound.PlaySound("sheep.wav", winsound.SND_ASYNC)
        elif self.numer == 7:
            self.picture = "sound.png"
            winsound.PlaySound("cow.wav", winsound.SND_ASYNC)
        elif self.numer == 8:
            self.picture = "sound.png"
            winsound.PlaySound("dog.wav", winsound.SND_ASYNC)
        elif self.numer == 9:
            self.picture = "sound.png"
            winsound.PlaySound("horse.wav", winsound.SND_ASYNC)
        elif self.numer == 10:
            self.picture = "sound.png"
            winsound.PlaySound("chicken.wav", winsound.SND_ASYNC)
        else:
            self.picture = "sound.png"
            winsound.PlaySound("pig.wav", winsound.SND_ASYNC)
        self.update()


    def tura(self):
        self.czy_test=0
        self.numer = random.randint(0,11)
        if self.numer == 0:
            self.picture = "sheep.jpg"
        elif self.numer==1:
            self.picture = "cow.jpg"
        elif self.numer==2:
            self.picture="dog.jpg"
        elif self.numer == 3:
            self.picture = "horse.jpg"
        elif self.numer == 4:
            self.picture = "chicken.png"
        elif self.numer == 5:
            self.picture = "pig.jpg"
        elif self.numer == 6:
            self.picture = "sound.png"
            winsound.PlaySound("sheep.wav", winsound.SND_ASYNC)
        elif self.numer == 7:
            self.picture = "sound.png"
            winsound.PlaySound("cow.wav", winsound.SND_ASYNC)
        elif self.numer == 8:
            self.picture = "sound.png"
            winsound.PlaySound("dog.wav", winsound.SND_ASYNC)
        elif self.numer == 9:
            self.picture = "sound.png"
            winsound.PlaySound("horse.wav", winsound.SND_ASYNC)
        elif self.numer == 10:
            self.picture = "sound.png"
            winsound.PlaySound("chicken.wav", winsound.SND_ASYNC)
        else:
            self.picture = "sound.png"
            winsound.PlaySound("pig.wav", winsound.SND_ASYNC)
        self.update()
        self.start = time.time()


    def stan(self):
        self.pixmap = QPixmap(self.picture)
        self.label12.setGeometry(350,200,300,210)
        self.label12.setPixmap(self.pixmap.scaled(300,210))

        self.label1.resize(150, 25)
        self.label1.move(800, 100 - 30)
        self.label1.setStyleSheet("QLabel { background-color : white; color : black; }")
        score = (str)(round(self.wyniki[0],6))
        self.label1.setText("Test numer 1 " + score)

        self.label2.resize(150, 25)
        self.label2.move(800, 125 - 30)
        self.label2.setStyleSheet("QLabel { background-color : white; color : black; }")
        score = (str)(round(self.wyniki[1],6))
        self.label2.setText("Test numer 2 " + score)

        self.label3.resize(150, 25)
        self.label3.move(800, 150 - 30)
        self.label3.setStyleSheet("QLabel { background-color : white; color : black; }")
        score = (str)(round(self.wyniki[2],6))
        self.label3.setText("Test numer 3 " + score)

        self.label4.resize(150, 25)
        self.label4.move(800, 175 - 30)
        self.label4.setStyleSheet("QLabel { background-color : white; color : black; }")
        score = (str)(round(self.wyniki[3],6))
        self.label4.setText("Test numer 4 " + score)

        self.label5.resize(150, 25)
        self.label5.move(800, 200 - 30)
        self.label5.setStyleSheet("QLabel { background-color : white; color : black; }")
        score = (str)(round(self.wyniki[4],6))
        self.label5.setText("Test numer 5 " + score)

        self.label6.resize(150, 25)
        self.label6.move(800, 225 - 30)
        self.label6.setStyleSheet("QLabel { background-color : white; color : black; }")
        score = (str)(round(self.wyniki[5],6))
        self.label6.setText("Test numer 6 " + score)

        self.label7.resize(150, 25)
        self.label7.move(800, 250 - 30)
        self.label7.setStyleSheet("QLabel { background-color : white; color : black; }")
        score = (str)(round(self.wyniki[6],6))
        self.label7.setText("Test numer 7 " + score)

        self.label8.resize(150, 25)
        self.label8.move(800, 275 - 30)
        self.label8.setStyleSheet("QLabel { background-color : white; color : black; }")
        score = (str)(round(self.wyniki[7],6))
        self.label8.setText("Test numer 8 " + score)

        self.label9.resize(150, 25)
        self.label9.move(800, 300 - 30)
        self.label9.setStyleSheet("QLabel { background-color : white; color : black; }")
        score = (str)(round(self.wyniki[8],6))
        self.label9.setText("Test numer 9 " + score)

        self.label10.resize(150, 25)
        self.label10.move(800, 325 - 30)
        self.label10.setStyleSheet("QLabel { background-color : white; color : black; }")
        score = (str)(round(self.wyniki[9],6))
        self.label10.setText("Test numer 10 " + score)

        self.update()

    def initUI(self):
        QToolTip.setFont(QFont('SansSerif', 10))

        self.label12 = QLabel(self)
        self.pixmap = QPixmap(self.picture)
        self.label12.setGeometry(350, 200, 300, 210)
        self.label12.setPixmap(self.pixmap.scaled(300, 210))


        btn1 = QPushButton('Reset', self)
        btn1.resize(btn1.sizeHint())
        btn1.clicked.connect(self.reset)
        btn1.move(80, 50)

        btn2 = QPushButton('Gra', self)
        btn2.clicked.connect(self.tura)
        btn2.resize(btn2.sizeHint())
        btn2.move(80, 75)

        btn3 = QPushButton('Wyjscie', self)
        btn3.clicked.connect(QApplication.instance().quit)
        btn3.resize(btn3.sizeHint())
        btn3.move(80, 100)

        btn10 = QPushButton('Trial', self)
        btn10.clicked.connect(self.test)
        btn10.resize(btn3.sizeHint())
        btn10.move(80, 125)
#-----------------------------------------------------------
        btn4 = QPushButton('Krowa', self)
        btn4.clicked.connect(self.cow)
        btn4.resize(btn4.sizeHint())
        btn4.move(375-30, 400)

        btn5 = QPushButton('Koń', self)
        btn5.clicked.connect(self.horse)
        btn5.resize(btn5.sizeHint())
        btn5.move(375-30, 450)

        btn6 = QPushButton('Kurczak', self)
        btn6.clicked.connect(self.chicken)
        btn6.resize(btn6.sizeHint())
        btn6.move(475-30, 400)

        btn7 = QPushButton('Swinia', self)
        btn7.clicked.connect(self.pig)
        btn7.resize(btn7.sizeHint())
        btn7.move(475-30, 450)

        btn8 = QPushButton('Owca', self)
        btn8.clicked.connect(self.sheep)
        btn8.resize(btn8.sizeHint())
        btn8.move(575-30, 400)

        btn9 = QPushButton('Pies', self)
        btn9.clicked.connect(self.dog)
        btn9.resize(btn9.sizeHint())
        btn9.move(575-30, 450)

#------------------------------------------------------------------

        self.label1 = QLabel(self)
        self.label1.resize(150, 25)
        self.label1.move(800, 100-30)
        self.label1.setStyleSheet("QLabel { background-color : white; color : black; }")
        score=(str)(self.wyniki[0])
        self.label1.setText("Test numer 1 " + score)

        self.label2 = QLabel(self)
        self.label2.resize(150, 25)
        self.label2.move(800, 125-30)
        self.label2.setStyleSheet("QLabel { background-color : white; color : black; }")
        score = (str)(self.wyniki[1])
        self.label2.setText("Test numer 2 " + score)

        self.label3 = QLabel(self)
        self.label3.resize(150, 25)
        self.label3.move(800, 150-30)
        self.label3.setStyleSheet("QLabel { background-color : white; color : black; }")
        score = (str)(self.wyniki[2])
        self.label3.setText("Test numer 3 " + score)

        self.label4 = QLabel(self)
        self.label4.resize(150, 25)
        self.label4.move(800, 175-30)
        self.label4.setStyleSheet("QLabel { background-color : white; color : black; }")
        score = (str)(self.wyniki[3])
        self.label4.setText("Test numer 4 " + score)

        self.label5 = QLabel(self)
        self.label5.resize(150, 25)
        self.label5.move(800, 200-30)
        self.label5.setStyleSheet("QLabel { background-color : white; color : black; }")
        score = (str)(self.wyniki[4])
        self.label5.setText("Test numer 5 " + score)

        self.label6 = QLabel(self)
        self.label6.resize(150, 25)
        self.label6.move(800, 225-30)
        self.label6.setStyleSheet("QLabel { background-color : white; color : black; }")
        score = (str)(self.wyniki[5])
        self.label6.setText("Test numer 6 " + score)

        self.label7 = QLabel(self)
        self.label7.resize(150, 25)
        self.label7.move(800, 250-30)
        self.label7.setStyleSheet("QLabel { background-color : white; color : black; }")
        score = (str)(self.wyniki[6])
        self.label7.setText("Test numer 7 " + score)

        self.label8 = QLabel(self)
        self.label8.resize(150, 25)
        self.label8.move(800, 275-30)
        self.label8.setStyleSheet("QLabel { background-color : white; color : black; }")
        score = (str)(self.wyniki[7])
        self.label8.setText("Test numer 8 " + score)

        self.label9 = QLabel(self)
        self.label9.resize(150, 25)
        self.label9.move(800, 300-30)
        self.label9.setStyleSheet("QLabel { background-color : white; color : black; }")
        score = (str)(self.wyniki[8])
        self.label9.setText("Test numer 9 " + score)

        self.label10 = QLabel(self)
        self.label10.resize(150, 25)
        self.label10.move(800, 325-30)
        self.label10.setStyleSheet("QLabel { background-color : white; color : black; }")
        score = (str)(self.wyniki[9])
        self.label10.setText("Test numer 10 " + score)

        #-------------------------------------------------------------------------------------





        self.setGeometry(200, 200, 1000, 620)
        self.setWindowTitle('Menu')
        self.setWindowIcon(QIcon('icon.jpg'))

        self.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
